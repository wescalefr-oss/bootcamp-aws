# Bootcamp AWS

Ce dépôt est un guide décrivant les étapes pour une montée en compétences opérationnelles sur AWS et un outillage qui nous semble pertinent. L'objectif finale de ce bootcamp est de se rôder à la gymnastique d'isolation de ressources sur AWS.

## Ready ?

Pour suivre l'intégralité des étapes, il vous faut :

* un compte gitlab avec un repository public où stocker le code que vous produirez.
* Terraform > 0.13
* Packer > 1.6
* Python > 3.7.3
* Ansible > 2.10
* awscli > 1.18
* [aws-vault](https://github.com/99designs/aws-vault)
* Une souscription AWS fraichement ouverte

## Set.

Quelques axiomes:
* TOUTES les directives données plus avant sont à coder. L'idée est d'avoir une approche Infrastructure-as-Code sans concession.
* Versionnez votre code, taggez le en utilisant le formalisme [SemVer](https://semver.org/lang/fr/) en commençant toujours vos tags par un "v" (ex: "v1.3.2").
* Sauf indication contraire, tout ce que vous pourrez utiliser comme module déjà existant sur le net est utilisable, du moment que vous allez regarder comment il fonctionne et que vous êtes capable de l'expliquer à un de vos pairs.
* Ce guide est un squelette de montée en compétence, vous aurez sûrement des choses à redire sur les premières étapes au fil de votre avancée, prenez des notes, envoyez des feedbacks.
* Ce guide s'adresse à des professionnels et suppose une capacité d'extrapolation et une certaine autonomie. Ne vous contentez pas des indices fournis, creusez les sujets.
* AKSK == Access Key Secret Key
* Pour le stockage des AKSK, privilégiez le passage par le fichier `~/.aws/config`, aws-vault et la variable d'environnement `AWS_PROFILE`.
* Mettez en place des [MFA](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa_enable_virtual.html#enable-virt-mfa-for-root) pour tous les accès que vous créez.

> Pour que cela vous soit profitable, projetez vous pour chaque étape dans l'exercice de faire une démo+code review de ce que vous aurez produit pour y arriver.

## Go!

### Premier administrateur

Vous démarrez avec le compte root de votre souscription AWS. Commencez par créer un compte IAM nommé `admin` et appliquez lui la policy 'Administrator'. 

Faites cela en utilisant une AKSK du compte root et en appliquant du code Terraform.

#### Indices

* https://github.com/WeScale/taco-recipe-aws-landing/blob/master/terraform/00-first-admin/main.tf

### Isolation de workload

Créer 2 souscriptions AWS liées à votre soucription initiale, nommées :

* `dev` qui servira pour tous les déploiement d'IaC en cours de développement
* `ops` qui servira de prod

#### Indices

* https://github.com/WeScale/taco-recipe-aws-landing/blob/master/terraform/00-organization/main.tf

> A partir de maintenant, n'utilisez plus le compte root pour interagir avec AWS, et pensez à ajouter un MFA dessus.

### Identification dans les bulles d'isolation

Retravaillez votre configuration de profile AWS pour être capable de facilement :

* vous identifier en tant qu'administrateur sur la souscription racine
* assumer un rôle administrateur dans la soucription `dev`
* assumer un rôle administrateur dans la soucription `ops`

#### Indices

* https://github.com/99designs/aws-vault#roles-and-mfa
* https://docs.aws.amazon.com/cli/latest/reference/sts/get-caller-identity.html

### Démo + Code review