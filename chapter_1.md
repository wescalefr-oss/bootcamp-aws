# Chapitre 1 : Infra-as-code 

Outils imposés :  Ansible, Terraform, script shell

## Initialisation de remote backend

Créer un script `setup_backend.sh` assurant :

* Création d'un bucket S3 chiffré server-side, refusant l'upload de fichiers non chiffrés
* Création d'un utilisateur IAM ayant les droit d'usage sur la clé KMS liée au bucket S3
* Changement du backend Terraform pour s'appuyer sur le bucket pour le stockage du tfstate

A la fin de la commande : 

* Le tfstate est présent sur le bucket
* Le user IAM créé peut le récupérer par un `aws s3 cp ...`

## Débranchement de remote backend

Créer un script assurant :

* Changement du backend Terraform pour stocker le tfstate en local
* Supression des ressources créées via `setup_backend.sh`

A la fin de la commande : 

* Le tfstate est présent en local

## Nota Bene

* Les 2 scripts doivent pouvoir s'enchainer de façon fluide.
* Portez une attention à la façon de transmettre les credentials AWS à vos commandes pour éviter qu'elles soient dans le code.
* Documentez au minimum pour qu'un nouvel arrivant puisse prendre en main votre projet.